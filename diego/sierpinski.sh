#!/bin/bash
# Barriga Martinez Diego Alberto

gen_len=151  
iterations=100
gen=()

function make_gen {
	for j in $(seq 0 $gen_len); do
		if [ $j -eq 50 ]; then
			$gen[$j]=\*
		else
			$gen[$j]=\_ 
		fi
	done
}

function generate_sierpinski {
# $1 is the base string
	c_gen=$1
	aux=$c_gen
	for j in $(seq 0 $iterations); do
		i=0
		while [ $i -lt $gen_len-1 ]; do
			if [ $c_gen[$i] = '*' -a [ $c_gen[$i-1] = '*' -a $c_gen[$i+1] = '*' ] ]; then
				$aux[$i]=\_ 
			elif [ $c_gen[$i-1] = '*' -o $c_gen[$i+1] = '*' ]; then
				$aux[$i]=\*
			fi
			$i+=1
		done
		printf "%s" "${aux[@]}"  # Print the array
		$c_gen=$aux
	done
}

echo "Comienza Sier"
make_gen
generate_sierpinski $gen


